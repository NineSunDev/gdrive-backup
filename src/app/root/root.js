const fs       = require('fs');
const rimraf   = require('rimraf');
const {google} = require('googleapis');
const sqldump  = require('mysqldump');
const config   = require('../../../config');
const logger   = require('../logger/logger');

const Root = {
  drive      : null,
  rootFolder : null,
  folderIds  : null,
  tmpDir     : './tmp',
  done       : 0,
  l          : logger,

  init : (auth) => {
    Root.l.init('./');
    Root.l.log('init root module');
    Root.drive = google.drive({version : 'v3', auth});


    Root.cleanTmpFolder(() => {
      Root.checkRootFolder((driveFile, db) => {
        Root.getMySQLDump(db, (file) => {
          Root.l.log('\'' + db + '/' + file + '\' uploading...');
          const fileMetadata = {
            name    : file,
            parents : [driveFile.id]
          };
          const media        = {
            mimeType : 'text/sql',
            body     : fs.createReadStream('./tmp/' + db + '/' + file)
          };
          Root.drive.files.create({
            resource : fileMetadata,
            media    : media,
          }, function (err) {
            if (err) {
              // Handle error
              console.error(err);
            } else {
              Root.done++;
              Root.l.log('\'' + db + '/' + file + '\' upload complete');
            }

            if (Root.done === config.dbs.length) {
              Root.l.log('Backup process finished');
            }
          });
        });
      });
    });
  },

  checkRootFolder : (cb) => {
    Root.l.log('check root folder');
    Root.drive.files.list({q : 'name = \'SQL-Backup for Ninesun.de\''}, (err, response) => {
      if (err) {
        Root.l.log(err, 2);
      } else {
        Root.rootFolder = response.data.files[0];
        Root.l.log(' root folder found with id: ' + Root.rootFolder.id);
        for (const db of config.dbs) {
          Root.drive.files.list({q : 'name = \'' + db + '\' and \'' + Root.rootFolder.id + '\' in parents'}, (err, response) => {
            if (err) {
              Root.l.log(err, 2);
            } else {
              cb(response.data.files[0], db);
            }
          });
        }
      }
    });
  },

  cleanTmpFolder : (cb) => {
    Root.l.log('cleaning tmp folder');
    rimraf(Root.tmpDir, () => {
      fs.mkdir(Root.tmpDir, () => {
        for (const db of config.dbs) {
          if (!fs.existsSync(Root.tmpDir + '/' + db))
            fs.mkdirSync(Root.tmpDir + '/' + db);
        }
        cb.apply();
      });
    });
  },

  getMySQLDump : async (db, cb) => {
    const n         = new Date();
    const timestamp = n.getFullYear() + '.' + ('0' + (n.getMonth() + 1)).slice(-2) + '.' + ('0' + n.getDate()).slice(-2);

    Root.l.log('\'' + db + '\' dumping...');
    await sqldump({
      connection : {
        host     : config.connection.host,
        user     : config.connection.user,
        password : config.connection.password,
        database : db,
      },
      dumpToFile : './tmp/' + db + '/' + timestamp + '.sql'
    }).then(() => {
      Root.l.log('\'' + db + '\' dump complete');
      cb(timestamp + '.sql');
    });
  }
};


module.exports = Root;
