const fs   = require('fs');

const Logger = {
  location : false,

  init : (location = false) => {
    Logger.location = location;
  },

  log : (message, type = 0) => {
    const pre = type === -1 ? '' : type === 0 ? 'INFO' : type === 1 ? 'WARN' : 'ERRO';
    const n = new Date();
    message =
      ('0' + n.getDate()).slice(-2) + '.' +
      ('0' + (n.getMonth() + 1)).slice(-2) + '.' +
      n.getFullYear() + ' ' +
      ('0' + n.getHours()).slice(-2) + ':' +
      ('0' + n.getMinutes()).slice(-2) + ':' +
      ('0' + n.getSeconds()).slice(-2) + '.' +
      ('00' + n.getMilliseconds()).slice(-3) + ' ['+pre+'] ' +
      message;
    console.log(
      (type === -1 ? '' : type === 0 ? '\x1b[36m' : type === 1 ? '\x1b[33m' : '\x1b[31m') +
      message + '\x1b[0m');
    if (Logger.location !== false) {
      fs.appendFileSync(Logger.location+'/gdb.log', message + '\n');
/*      if(!fs.existsSync(location+'/gdb.log')) {
        fs.
      }*/
    }
  }
};


module.exports = Logger;
